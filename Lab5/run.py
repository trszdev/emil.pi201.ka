#!/usr/bin/env python3
# coding=utf8

'''
Найти, если оно есть, полное паpосочетание в двудольном гpафе.
Метод решения: сведение к задаче о максимальном потоке и использование поиска в глубину
для поиска f-дополняющих цепей (М-чеpедующихся).

Файл входных данных: двудольный гpаф G=(X,Y,E), k=|X|, l=|Y|,
заданный Х-списками смежностей.
X-списки смежностей - k штук списков смежностей по одному для каждой вершины x из X.
В пеpвой стpоке файла числа k l.

Файл выходных данных: Y и во второй строке полное паросочетание, представленное массивом
XПАРА, или N и во второй строке вершина xi, из которой поиск не удачен.
'''


def read_graph():
    with open('in.txt') as file:
        n, k = map(int, file.readline().split())
        g = [[] for _ in range(n + 1)]
        for i, line in enumerate(file):
            verts = list(map(int, line.split()))
            g[i + 1] = verts[:-1]
        return n + 1, k + 1, g


n, k, g = read_graph()
mt = [0] * k


def try_kuhn(v, used):
    if not used[v]:
        used[v] = 1
        for xi in g[v]:
            if not mt[xi] or try_kuhn(mt[xi], used):
                mt[xi] = v
                return True
    return False


with open('out.txt', 'w') as file:
    for v in range(1, n):
        if not try_kuhn(v, [0] * n):
            print('N', file=file)
            print(v, file=file)
            break
    else:
        print('Y', file=file)
        pairs = list(enumerate(mt))[1:]
        pairs.sort(key=lambda x: x[0])
        print(' '.join(str(y) for x, y in pairs), file=file)
