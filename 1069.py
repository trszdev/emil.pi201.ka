from heapq import heappop, heappush
from sys import stdin

vs = list(map(int, stdin.read().split()))
n = len(vs) + 2

lastv = [-1] * n

for i, v in enumerate(vs):
    lastv[v] = i

heap = [i for i in range(1, n) if lastv[i] == -1]

adj = [[] for _ in range(n)]

for i, v in enumerate(vs):
    w = heappop(heap)
    heappush(adj[v], w)
    heappush(adj[w], v)
    if lastv[v] == i:
        heappush(heap, v)

for v in range(1, n):
    adjacent = map(str, adj[v])
    print('%d: %s' % (v, ' '.join(adjacent)))
