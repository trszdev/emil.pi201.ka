#!env python2

# http://acm.timus.ru/problem.aspx?space=1&num=1198

def build_condensed(g):
    return g


n = input()
n += 1
MAX_N = 2000 + 1

adj = [[] for _ in xrange(n)]
degs_out = [0] * n
degs_in = [0] * n

for i in xrange(1, n):
    vs = map(int, raw_input().split())
    for v in vs:
        degs_out[i] += 1
        degs_in[v] += 1
        adj[i].append(v)


weaks = 0
out_leaves = 0
visited = [0] * n
out_leaf = -1

for v in xrange(1, n):
    if visited[v]: continue
    q = [v]
    weaks += 1
    while q: 
        w = q.pop()
        if visited[w]: continue
        if degs_in[w] == 0 and degs_out[w] > 0: 
            out_leaves += 1
            out_leaf = w
        visited[w] = 1
        q.extend(adj[w])


if weaks > 1 or out_leaves > 2: 
    print 0; exit()

if out_leaves:
    print '%d 0' % out_leaf; exit()






