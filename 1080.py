n = int(input()) + 1

adj = [[] for _ in range(n)]
for i in range(1, n):
    vs = list(map(int, input().split()[:-1]))
    for v in vs:
        adj[v].append(i)
    adj[i].extend(vs)

used = [0] * n
colors = [False] * n

def dfs(v, is_red=True):
    if used[v]:
        for w in adj[v]:
            if used[w] and colors[w] == is_red:
                return False
    else:
        used[v] = 1
        colors[v] = is_red
        for w in adj[v]:
            if not dfs(w, not is_red):
                return False
    return True

if dfs(1, is_red=True):
    print(''.join('0' if x else '1' for x in colors[1:]))
else:
    print(-1)
