#!env python2

# http://acm.timus.ru/problem.aspx?space=1&num=1314


get_nums = lambda: map(int, raw_input().split())
MAX_K = 32767+1
n = input()
adj = {x:[] for x in xrange(MAX_K)}


for stations in (get_nums() for _ in xrange(n)):
    last = stations[1]
    for station in stations[2:]:
        adj[last].append(station)
        adj[station].append(last)
        last = station


visited = [0] * MAX_K
path_len = [0] * MAX_K
path_len2 = [0] * MAX_K
criminal_path = get_nums()
m = criminal_path.pop(0)

q = [criminal_path[0]]
visited[q[0]] = 1
while q:
    v = q.pop(0)
    for w in adj[v]:
        if visited[w]: continue
        visited[w] = 1
        path_len[w] = path_len[v] + 1
        q.append(w)

visited = [0] * MAX_K
for station in criminal_path[:-1]:
    visited[station] = 1
    path_len2[station] = path_len[station] + 1 
    
path_len2[criminal_path[-1]] = m - 1

q = [criminal_path[-1]]
visited[q[0]] = 1
while q:
    v = q.pop(0)
    for w in adj[v]:
        if visited[w]: continue
        visited[w] = 1
        path_len2[w] = path_len2[v] + 1
        q.append(w)



for i in xrange(MAX_K):
    if visited[i] and path_len2[i] <= path_len[i]: 
        print i

