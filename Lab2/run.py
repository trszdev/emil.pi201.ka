#!env python2
#coding=utf8

"""
Определить, является ли данный граф ацикличным.
Метод решения: поиск в глубину.
Файл исходных данных: граф, заданный матрицей смежности.
*Предполагается, что граф связный и неориентированный,
а длина цикла больше 1, т.е A-B-A не явл. циклом

Пример файла данных:
     4
     0  1  1  0
     1  0  1  0
     1  1  0  1
     0  0  1  0

Файл результатов:
    Если граф ацикличен, то в файл результатов необходимо записать
    "A"(латинское), иначе "N" и далее вершины входящие в первый найденный цикл.
    Вершины в цикле должны быть упорядочены по возрастанию номеров.
"""

lines = open('in.txt', 'r').read().splitlines()

n = int(lines.pop(0))
adj = [[] for _ in xrange(n)]

for i, line in enumerate(lines):
    for j, is_adj in enumerate(map(int, line.split())):
        if is_adj: 
            adj[i].append(j)


visited = [0] * n
cycle = []
route = {}
q = [0]
route[0] = -1
while q and not cycle:
    v = q.pop()
    visited[v] = 1
    for w in adj[v]:
        if w == route[v]: continue
        if visited[w]:
            last = v
            while last != w:
                cycle.append(last+1)
                last = route[last]
            cycle.append(w+1)
            break
        route[w] = v
        q.append(w)


out = open('out.txt', 'w')
if cycle:
    cycle.sort()
    print >>out, 'N'
    print >>out, ' '.join(map(str, cycle)),
else:
    print >>out, 'A',

