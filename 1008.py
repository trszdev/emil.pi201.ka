nums = tuple(map(int, input().split()))

directions = { 'R': (1, 0), 'T': (0, 1), 'L': (-1, 0), 'B': (0, -1) }


def case2(point):
    stack = [point]
    points = [point]
    while stack:
        x, y = stack.pop(0)
        line = input()[:-1]
        if not line:
            continue
        for ox, oy in map(lambda x: directions[x], line):
            opoint = ox + x, oy + y
            stack.append(opoint)
            points.append(opoint)
    points.sort()
    print(len(points))
    for p in points:
        print('%d %d' % p)


def case1(n):
    used = set()
    points = [tuple(map(int, input().split())) for _ in range(n)]
    stack = []
    if points:
        print('%d %d' % points[0])
        stack.append(points[0])
        used.add(points[0])
    points = set(points)
    while stack:
        x, y = stack.pop(0)
        for side, (ox, oy) in directions.items():
            point = ox + x, oy + y
            if point in points and point not in used:
                print(side, end='')
                used.add(point)
                stack.append(point)
        if stack:
            print(',')
    print('.')

if len(nums) == 1:
    case1(nums[0])
else:
    case2(nums)
