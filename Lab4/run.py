#!/usr/bin/env python3
# coding=utf8

'''
Построить минимальный остов связного неориентированного взвешенного
графа. Метод решения: алгоритм Ярника-Прима-Дейкстры.

Файл входных данных: граф, заданный матрицей весов.
В первой строке записано количество вершин.
Далее построчно расположена матрица весов размерности NxN.
Число 32767 означает, что данное ребро отсутствует.
 
Файл выходных данных: остов T, заданный списками смежностей (для каждой вершины указываются
смежные с ней вершины, вершины отделяются друг от друга пробелами, в конце
списков смежных вершин нули, каждый список с новой строки). Внутри каждого
списка вершины упорядочить по возрастанию номеров. В последней строке файла записать вес (|T|).
'''


MAGIC = 32767

total_weight = 0
n = 0
answer = []

with open('in.txt', 'rb') as file:
    costs = [[0] + list(map(int, x.split())) for x in file.read().splitlines()]
    n = len(costs)
    used = [False] * n
    min_weight = [MAGIC] * n
    min_vertex = [0] * n
    answer = [[] for _ in range(n)]
    for i in range(1, n):
        v = 0
        for j in range(1, n):
            if not used[j] and (not v or min_weight[j] < min_weight[v]):
                v = j
        used[v] = True
        if min_vertex[v]:
            total_weight += min_weight[v]
            answer[v].append(min_vertex[v])
            answer[min_vertex[v]].append(v)
        for j in range(1, n):
            if costs[v][j] < min_weight[j]:
                min_weight[j] = costs[v][j]
                min_vertex[j] = v

with open('out.txt', 'w') as file:
    for i in range(1, n):
        vertices = list(sorted(answer[i])) + [0]
        file.write(' '.join(map(str, vertices)) + '\n')
    file.write(str(total_weight))
