#!env python2

# http://acm.timus.ru/problem.aspx?space=1&num=1272

def get_nums():
    return map(int, raw_input().split())


n, k, m = get_nums()

adj = [[] for x in range(n+1)]
belongs = [0] * (n+1)


answer = 0
for x, y in (get_nums() for _ in xrange(k)):
    adj[x].append(y)
    adj[y].append(x)


for v in range(1, n + 1):
    if belongs[v]: continue
    answer += 1
    q = [v]
    while q:
        t = q.pop()
        if belongs[t]: continue
        belongs[t] = 1
        q.extend(adj[t])


print answer - 1


