#!env python2
#coding=utf8

"""
Найти v-w путь в сети с неотрицательными весами с max весом.
Вес пути это произведение весов дуг, входящих в путь.
Метод решения: алгоритм Форда-Беллмана.

Файл входных данных:
В первой строке N. В i-ой строке записан список инцидентных вершин для i вида
(вершина вес), список завершается нулем. В конце файла записаны источник и цель.
Пример:
    4
    2 25 3 4 0
    0
    2 0 4 7 0
    0
    1
    4

Файл выходных данных:
В случае отсутствия пути вывести "N"; 
иначе "Y", путь от источника, вес данного пути в отдельных строках
"""
import sys
stdin = open('in.txt', 'r')
stdout = sys.stdout
stdout = open('out.txt', 'w')

lines = stdin.read().splitlines()

n = int(lines.pop(0))
adj = [[] for _ in xrange(n)]
weights = [[0] * n for _ in xrange(n)] 


for i, line in enumerate(lines):
    nums = map(int, line.split())
    for v, weight in zip(nums[::2], nums[1::2]):
        weights[i][v-1] = weight
        adj[i].append(v-1)


source, target = map(int, lines[-2:])
source -= 1
target -= 1
ptrack = [-1] * n
dists = [-1] * n
dists[source] = 0


for v in adj[source]:
    dists[v] = weights[source][v]
    ptrack[v] = source


for i in xrange(n - 1):
    for v in xrange(n):
        for w in adj[v]:
            new_len = dists[v] * weights[v][w]
            if dists[w] < new_len:
                dists[w] = new_len
                ptrack[w] = v


path = []
last = target
while last != -1:
    path.append(last)
    last = ptrack[last]
path.reverse()

has_path = target in path and source in path

if has_path:
    print >>stdout, "Y"
    print >>stdout, ' '.join(str(x+1) for x in path)
    print >>stdout, dists[target],
else: 
    print >>stdout, "N",
