#!env python2
# coding=utf8

"""
На шахматной доске стоят белый конь и черная пешка.  
Напечатать маршрут коня позволяющий уничтожить пешку.
Примечание: пешка - неподвижная, конь не должен попадать под удар пешки.
Метод решения: Поиск в ширину.

Сначала располагаются координаты коня затем пешки.  Координаты даются
в шахматной нотации,  т.е.  в виде AB, где A может принимать значения от a
до h, B от 1 до 8. Пример: 
b2
e6

Формат файла результатов: Маршрут в шахматной нотации.
Маршрут должен начинаться координатами коня и заканчиваться
координатами пешки. Каждый ход записывается с новой строки. 
"""

_letters = 'abcdefgh'

def parse_position(spos):
    x = _letters.index(spos[0])
    y = int(spos[1])
    return x, y - 1

def is_correct_pos(x, y):
    return 0 <= x <= 7 and 0 <= y <= 7

def translate_position(pos):
    x, y = pos
    return '%s%d' % (_letters[x], y + 1)


knight, pawn = open('in.txt', 'r').read().splitlines()
knight, pawn = map(parse_position, [knight, pawn])
visited = [[0] * 8 for _ in range(8)]
found = 0

if is_correct_pos(pawn[0] + 1, pawn[1] - 1):
    visited[pawn[0] + 1][pawn[1] - 1] = 1

if is_correct_pos(pawn[0] - 1, pawn[1] - 1):
    visited[pawn[0] - 1][pawn[1] - 1] = 1


q = [knight]
routes = { knight: -1 }
visited[knight[0]][knight[1]] = 1
while q:
    x, y = q.pop(0)
    if (x, y) == pawn: found = 1; break
    offsets = [(-1, 2), (1, 2),
               (2, 1), (2, -1),
               (-1, -2), (1, -2),
               (-2, -1), (-2, 1)]
    for offset in offsets:
        nx, ny = (x+offset[0], y+offset[1])
        if is_correct_pos(nx, ny) and not visited[nx][ny]:
            visited[nx][ny] = 1
            q.append((nx, ny))
            routes[(nx, ny)] = (x, y)

file = open('out.txt', 'w')
if not found:
    print >>file, 'Can\'t find path for knight'
else:
    last = pawn
    path = []
    while last != -1:
        path.append(last)
        last = routes[last]
    path.reverse()
    print >>file, '\n'.join(map(translate_position, path)),
